# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Httpd
      module Build
        def self.update_httpd
          file = '/etc/apache2/httpd.conf'
          text = File.read(file)
          text = text.gsub(%r{^#LoadModule[\s]+negotiation_module[\s]+.*$}i, '')
          text = text.gsub(%r{^[#\s]*(LoadModule[\s]+.*)$}i, '# \1')
          text = text.gsub(%r{^[#\s]*(LoadModule)[\s]+(mpm_event|authz_core|dir|mime|negotiation|unixd|rewrite)(_module[\s]+.*)$}i, '\1 \2\3')
          text = text.gsub(%r{^[#\s]*(User|Group)[\s]+.*$}i, '\1 core')
          text = text.gsub(%r{^[#\s]*(ServerName)[\s]+[.:\w]+$}i, '\1 127.0.0.1')
          text = text.gsub(%r{([\s]+"/var/www/)localhost/htdocs(/?")}i, '\1html\2')
          text = text.gsub(%r{([\s]+"/var/www/)localhost/(cgi-bin/?")}i, '\1\2')
          text += "IncludeOptional /usr/local/etc/apache2/*.conf \n"
          File.write(file, text)
        end

        def self.update_http2
          file = '/etc/apache2/conf.d/http2.conf'
          text = File.read(file)
          text += "Protocols h2 h2c http/1.1 \n"
          File.write(file, text)
        end

        def self.update_proxy
          file = '/etc/apache2/conf.d/proxy.conf'
          text = File.read(file)
          text = text.gsub(%r{^[#\s]*(LoadModule[\s]+.*)$}, '# %1')
          File.write(file, text)
        end

        def self.main
          System.run('apk add --no-cache apache2 apache2-proxy apache2-http2')
          Shell.make_folders('/usr/local/etc/apache2')
          Shell.move_paths('/var/www/localhost/*', '/var/www/')
          File.rename('/var/www/htdocs', '/var/www/html')
          Shell.remove_paths('/var/www/localhost')
          Shell.link_logs(stdout: '/var/log/apache2/access.log', stderr: '/var/log/apache2/error.log')
          System.invoke('Update httpd', self.method(:update_httpd))
          System.invoke('Update http2', self.method(:update_http2))
          System.invoke('Update proxy', self.method(:update_proxy))
        end
      end

      module Run
        def self.update_httpd
          root = ENV.fetch('HTTPD_DOCUMENT_ROOT', '')

          file = '/etc/apache2/httpd.conf'
          text = File.read(file)

          if false == root.empty?
            Shell.make_folders("/var/www/html/#{root}")
            text = text.gsub(%r{([\s]+"/var/www/html)(/?")}i, '\1/' + root + '\2')
          end

          File.write(file, text)
        end

        def self.main
          Shell.update_user
          System.invoke('Update httpd', self.method(:update_httpd))

          Shell.clear_folders('/run/apache2')
          Shell.change_owner('/var/www/cgi-bin', '/var/www/html')
          System.execute('httpd', { D: 'FOREGROUND' }, '')
        end
      end
    end
  end
end
