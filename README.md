# Summary

Source: https://gitlab.com/agrozyme-docker/httpd

The Apache HTTP Server Project

# Settings

- MPM: Event
- DocumentRoot Directory: `/var/www/html`
- Default Configuration Directory: `/etc/apache2`
- Custom Configuration Files: `/usr/local/etc/apache2/*.conf`

# Environment Variables

When you start the image, you can adjust the configuration of the instance by passing one or more environment variables on the docker run command line.

- HTTPD_DOCUMENT_ROOT

## HTTPD_DOCUMENT_ROOT

This variable is optional and allows you to specify the path in `/var/www/html` for `DocumentRoot`.
ex: set `HTTPD_DOCUMENT_ROOT` = `root`, the `DocumentRoot` is `/var/www/html/root`
